# Talakay: A Guide to the Model-View-Controller Architecture

## Introduction

Hi everyone!

This repository is dedicated to `Talakay` an application I built to demonstrate the basics of the Model-View-Controller Architecture. 
`Talakay` is basically a simpler version of Reddit. This repository was made for educational purposes, please don't use this for commercial purposes.

`Talakay` is a simple login and posting app. 
Due to time constraints, I wasn't able to push more features.

However, the code shows at least how the MVC Architecture works.

## Model-View-Controller-Architecture

The Model-View-Controller Architecture tries to separate the code responsibilities into three parts:

* The `Model` which is in charge of the data.
* The `View` which in charge of what is shown to the client or user.
* The `Controller` which acts as a mediator between these two.

### Model
The `Model` is in charge of the data, it holds how the data is stored and how it is manipulated
For instance, in the code, our Main Models would be `User` and `Discussion`.
These are two very simple Models but if we add more logic like: showing all posts except from specific users `fetchDiscussionsNotFromUser(User[] users)` would be located in `DiscussionRepository`.

### View
The `View` is basically what is shown to the client or the users. Resource packs such as icons and fonts are usually stored in this folder.

### Controller
The `Controller` acts as a mediator between the Model and the View. Usually, this is what binds the data and the view.
There are two types on how data is pushed to the view.

#### MVC Push Method
The model will be responsible for updates to the controller. The scenario here is:

1. `Model` receives an update from the data, sends it to the `Controlller`
2. `Controller` receives update and sends it to the `View`.

This will work well for models that are constantly updated especially by other factors like other users.
We implement through a list called `Observers`. These `Observers` will be the one called if there are any updates to the data.
You may view `DiscussionRepository` and `UserRepository`. Both have `Observers` under their classes.

#### MVC Pull Method
The view will be responsible for fetching data from the controller. The scenario here is:

1. `View` asks data from the `Controller`
2. `Controller` taps `Model` to fetch for latest data.
3. `Model` returns the latest data to `Controller`
4. `Controller` finally sends the data back to the `View`

In the app, I mostly used `push` since almost everything is a user action initiated. 
Although, I tried to used the `push` method for fetching all posts. It is ideal to still use the `pull` method.

This will work well for requests. For instance, if a user needs to fetch all posts that has his username. That is a pull method. 
Don't stick to only one of these architectures. You may implement a both of them at the same time.

It is a case to case basis on when the `pull` or `push` method will be used.

The `push` method is best suited for `user actions`.

The `pull` method is best suited for `requests`.

## Other Design Patterns Applied
 The Model-View-Controller is what we call a design pattern.
 I also used other design patterns not related to MVC.
 
 ### Delegation Pattern
 The pattern is commonly used for mobile or desktop applications where in the responsibility of user actions will be delegated and passed up to the hierarchy.
 I used this very often for components that perform certain actions.
 
 `onSignIn()` from `TopBarPanel` delegated up to `TalakayDesktopApp`
 
 You may refer to this link: http://best-practice-software-engineering.ifs.tuwien.ac.at/patterns/delegation.html for further information.
 
 ### Singleton Pattern
 A design pattern where in you want to use only `ONE` instance of an object throughout your code. This is commonly `abused` by many developers. 
 For further information on when and why to use this pattern, please refer to link: https://www.dofactory.com/forum/1568/singleton-pattern-use.
 Mostly I used this in the Resource Managers: `IconManager` and `FontManager`.

## Questions?
If you have any questions, clarifications or corrections about the repository or the MVC architecture:

You may contact me at: `jonalrayticug@gmail.com` or leave a message on Facebook.
You may also leave a question through the issues here at `Gitlab` so that everyone can see.

Thanks!

## Resources:
* `resources.fonts.Roboto:` https://fonts.google.com/specimen/Roboto
* `Pull v.s. Push MVC:` https://www.guyrutenberg.com/2008/04/26/pull-vs-push-mvc-architecture/