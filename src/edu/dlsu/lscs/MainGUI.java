/*
 * Copyright (c) 2020. Jonal Ray Ticug
 * All Rights Reserved.
 */
package edu.dlsu.lscs;

import edu.dlsu.lscs.controller.TalakayAppController;

/**
 * Run this class to view a console command
 */
public class MainGUI {

  public static void main (String[] args) {
    TalakayAppController appController = new TalakayAppController();
    appController.start();
  }
}
