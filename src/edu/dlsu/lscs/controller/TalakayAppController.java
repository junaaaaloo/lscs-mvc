package edu.dlsu.lscs.controller;

import edu.dlsu.lscs.model.discussions.Discussion;
import edu.dlsu.lscs.model.discussions.DiscussionRepository;
import edu.dlsu.lscs.model.user.UserRepository;
import edu.dlsu.lscs.view.desktop.TalakayDesktopApp;
import edu.dlsu.lscs.view.desktop.components.DiscussionList;
import edu.dlsu.lscs.view.desktop.components.DiscussionsPanel;
import edu.dlsu.lscs.view.desktop.components.TopBarPanel;

import java.util.List;

/**
 * This wiill be our Main Application Controller
 *
 */
public class TalakayAppController implements TopBarPanel.TopBarPanelDelegate, DiscussionsPanel.DiscussionsPanelDelegate, DiscussionList.DiscussionFetcher {
  // Main View
  private TalakayDesktopApp view;

  // Data Source/s OR Models
  private DiscussionRepository discussionRepository;
  private UserRepository userRepository;

  public TalakayAppController() {
    this.view = new TalakayDesktopApp(this);
    this.userRepository = new UserRepository(view.getTopBarPanel());
    this.discussionRepository = new DiscussionRepository(userRepository, view.getDiscussionsList());

    discussionRepository.loadDiscussions();
  }

  public void start () {
    view.run();
  }

  // Discussions Panel Delegate

  @Override
  public void onLogout() {
    userRepository.logout();
  }

  @Override
  public void onSignIn(String username) {
    userRepository.setUser(username);
  }

  @Override
  public void onPost(String content) {
    discussionRepository.addPostFromContent(content);
  }

  @Override
  public List<Discussion> getDiscussions() {
    return discussionRepository.getDiscussions();
  }
}
