/*
 * Copyright (c) 2020. Jonal Ray Ticug
 * All Rights Reserved.
 */
package edu.dlsu.lscs.model.discussions;

import edu.dlsu.lscs.model.user.User;

public class Discussion {
  private String content;
  private User createdBy;

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public User getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(User createdBy) {
    this.createdBy = createdBy;
  }

}
