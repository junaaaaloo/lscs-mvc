package edu.dlsu.lscs.model.discussions;

import edu.dlsu.lscs.model.user.User;
import edu.dlsu.lscs.model.user.UserRepository;
import edu.dlsu.lscs.view.desktop.components.DiscussionsPanel;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class DiscussionRepository  {
  public interface DiscussionObserver {
    void add(Discussion discussion);
  }

  private final UserRepository userRepository;
  private final List<Discussion> discussions;
  private final DiscussionObserver observer;

  public DiscussionRepository(UserRepository userRepository, DiscussionObserver observer) {
    discussions = new ArrayList<>();
    this.userRepository = userRepository;
    this.observer = observer;
  }

  public void loadDiscussions () {
    User model = new User();
    model.setUsername("MODEL");

    Discussion discussion1 = new Discussion();
    discussion1.setCreatedBy(model);
    discussion1.setContent("Responsible for Handling the Data, and where the data should be stored.");

    User view = new User();
    view.setUsername("VIEW");

    Discussion discussion2 = new Discussion();
    discussion2.setCreatedBy(view);
    discussion2.setContent("Responsible for Showing the Data, Resource Icons and Fonts");

    User controller = new User();
    controller.setUsername("CONTROLLER");

    Discussion discussion3 = new Discussion();
    discussion3.setCreatedBy(controller);
    discussion3.setContent("Acts as a mediator between the the Model and the View");

    add(discussion1);
    add(discussion2);
    add(discussion3);
  }

  public void add (Discussion discussion) {
    discussions.add(discussion);
    observer.add(discussion);
  }

  public void addPostFromContent(String content) {
    User user = userRepository.getUser();

    if (user == null) {
      return;
    }

    Discussion discussion = new Discussion();
    discussion.setContent(content);
    discussion.setCreatedBy(user);

    add(discussion);
  }

  public List<Discussion> getDiscussions () {
    return discussions;
  }
}
