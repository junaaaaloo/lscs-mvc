/*
 * Copyright (c) 2020. Jonal Ray Ticug
 * All Rights Reserved.
 */
package edu.dlsu.lscs.model.user;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.logging.Logger;

public class User {
  private static final Logger LOG  = Logger.getLogger(User.class.getName());
  private String username;
  private Timestamp lastLoggedIn;

  // === Utility Methods ===

  public Timestamp login () {
    this.lastLoggedIn = Timestamp.from(Instant.now());
    return lastLoggedIn;
  }

  public static User register (String username) {
    User user = new User();
    user.username = username;
    Timestamp timestamp = user.login();
    LOG.info("User logged in: @" + username + " at " + timestamp.toString());

    return user;
  }

  // === Getters and Setters ===

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Timestamp getLastLoggedIn() {
    return lastLoggedIn;
  }
}
