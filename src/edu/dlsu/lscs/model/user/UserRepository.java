package edu.dlsu.lscs.model.user;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {
  public interface UserObserver {
    void update(User user);
  }

  private UserObserver observer;
  private User user;

  public UserRepository (UserObserver observer) {
    this.observer = observer;
  }

  public void setUser(String username) {
    User user = new User();
    user.setUsername(username);
    this.user = user;

    user.login();

    observer.update(user);
  }

  public void logout () {
    this.user = null;

    observer.update(null);
  }

  public User getUser() {
    return user;
  }
}
