/*
 * Copyright (c) 2020. Jonal Ray Ticug
 * All Rights Reserved.
 */
package edu.dlsu.lscs.view;

import edu.dlsu.lscs.model.user.UserRepository;
import edu.dlsu.lscs.view.desktop.components.DiscussionsPanel;

public abstract class BaseApp {
  private boolean running;

  /**
   * The child class inheriting the method should
   * implement this to initialize all starting components.
   */
  protected abstract void initialize();

  /**
   * This will start the GUI.
   * If the GUI is already running,
   */
  protected abstract void open();

  /**
   * The child class inheriting the method should
   * implement this in such a way the GUI will stop.
   * In stopping the GUI, it should be possible to open it again.
   */
  protected abstract void close();

  public BaseApp () {
    running = false;
  }

  public void run() {
    // If it is running, no point in starting the app
    if (running) {
      return;
    }

    running = true;
    open();
  }

  public void stop() {
    // If it is not running, no point in closing the app
    if (!running) {
      return;
    }

    running = false;
    close();
  }

  public boolean isRunning() {
    return running;
  }

  public void setRunning(boolean running) {
    this.running = running;
  }
}
