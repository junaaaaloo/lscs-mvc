/*
 * Copyright (c) 2020. Jonal Ray Ticug
 * All Rights Reserved.
 */
package edu.dlsu.lscs.view;

import java.awt.*;

/**
 * To define the default height and width of the all the other apps.
 * May or may not be used by the Main View JFrame
 */
public class ViewConstants {
  public static final int DESKTOP_HEIGHT = 700;
  public static final int DESKTOP_WIDTH = 800;

  // FONTS
  public static final String PRIMARY_FONT_PATH = "resources/fonts/Roboto/Roboto-Regular.ttf";

  // COLORS
  public static final Color primaryColor = Color.decode("#305F72");
  public static final Color accentColor = Color.decode("#F1D1B5");

  public static final Color text = Color.black;
  public static final Color lightText = Color.white;
  public static final Color grayText = Color.darkGray;
}
