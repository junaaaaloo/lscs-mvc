package edu.dlsu.lscs.view.components;

import edu.dlsu.lscs.view.utility.FontManager;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;

public class MaterialJButton extends JButton {
  public MaterialJButton () {
    setFont(FontManager.primaryFont().deriveFont(12.f));
  }
}
