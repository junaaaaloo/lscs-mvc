package edu.dlsu.lscs.view.components;

import edu.dlsu.lscs.view.utility.FontManager;

import javax.swing.*;
import java.awt.*;

public class MaterialJLabel extends JLabel {
  public MaterialJLabel () {
    setFont(FontManager.primaryFont().deriveFont(12.f));
  }
}
