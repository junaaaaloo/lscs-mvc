package edu.dlsu.lscs.view.components;

import edu.dlsu.lscs.view.ViewConstants;
import edu.dlsu.lscs.view.utility.FontManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.geom.RoundRectangle2D;

public class MaterialJTextField extends JTextField {
  private Shape shape;

  public MaterialJTextField () {
    setFont(FontManager.primaryFont().deriveFont(14.f));
    setOpaque(false);
  }

  protected void paintComponent(Graphics g) {
    g.setColor(getBackground());
    g.fillRoundRect(0, 0, getWidth()-1, getHeight()-1, 5, 5);
    super.paintComponent(g);
  }

  protected void paintBorder(Graphics g) {
    g.drawRoundRect(0, 0, getWidth()-1, getHeight()-1, 5, 5);
  }

  public boolean contains(int x, int y) {
    if (shape == null || !shape.getBounds().equals(getBounds())) {
      shape = new RoundRectangle2D.Float(0, 0, getWidth()-1, getHeight()-1, 5, 5);
    }
    return shape.contains(x, y);
  }
}
