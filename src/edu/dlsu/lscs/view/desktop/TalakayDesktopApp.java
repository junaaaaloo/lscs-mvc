/*
 * Copyright (c) 2020. Jonal Ray Ticug
 * All Rights Reserved.
 */
package edu.dlsu.lscs.view.desktop;

import edu.dlsu.lscs.controller.TalakayAppController;
import edu.dlsu.lscs.model.discussions.Discussion;
import edu.dlsu.lscs.model.user.UserRepository;
import edu.dlsu.lscs.view.ViewConstants;
import edu.dlsu.lscs.view.BaseApp;
import edu.dlsu.lscs.view.desktop.components.DiscussionList;
import edu.dlsu.lscs.view.desktop.components.DiscussionsPanel;
import edu.dlsu.lscs.view.desktop.components.TopBarPanel;
import edu.dlsu.lscs.view.utility.IconManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class TalakayDesktopApp extends BaseApp {
  private TopBarPanel topBar;
  private DiscussionsPanel discussions;

  private JFrame mainFrame;

  // Connection to Controller
  private TalakayAppController controller;

  public static String TITLE = "Talakay | Desktop";

  public TalakayDesktopApp(TalakayAppController controller) {
    this.controller = controller;
    initialize();
  }

  // Setup Methods
  @Override
  protected void initialize() {
    initializeFrame();
    initializeDiscussions();
    initializeTopBar();
    initializeLayout();
  }

  private void initializeDiscussions () {
    discussions = new DiscussionsPanel(this.controller);
  }

  private void initializeTopBar () {
    topBar = new TopBarPanel(this.controller);
  }

  private void initializeLayout () {
    mainFrame.add(topBar, BorderLayout.NORTH);
    mainFrame.add(discussions, BorderLayout.CENTER);
  }

  public void initializeFrame() {
    mainFrame = new JFrame(TITLE);

    mainFrame.setLayout(new BorderLayout());
    mainFrame.setIconImage(IconManager.icon());

    mainFrame.setResizable(false);
    mainFrame.setSize(ViewConstants.DESKTOP_WIDTH, ViewConstants.DESKTOP_HEIGHT);

    mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    mainFrame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        super.windowClosed(e);
        close();
      }
    });
  }

  // Getters

  public TopBarPanel getTopBarPanel() {
    return topBar;
  }

  public DiscussionList getDiscussionsList() {
    return discussions.getDiscussionList();
  }

  // Overridden Methods

  @Override
  protected void open() {
    mainFrame.setVisible(true);
  }

  @Override
  protected void close() {
    mainFrame.setVisible(false);
  }

}
