package edu.dlsu.lscs.view.desktop.components;

import edu.dlsu.lscs.view.ViewConstants;
import edu.dlsu.lscs.view.components.MaterialJButton;
import edu.dlsu.lscs.view.components.MaterialJLabel;
import edu.dlsu.lscs.view.components.MaterialJTextArea;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.View;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddDiscussionForm extends JPanel {
  public interface AddDiscussionFormDelegate {
    void onPost(String content);
  }

  private MaterialJLabel postLabel;
  private MaterialJTextArea postBody;
  private MaterialJButton postButton;

  private final AddDiscussionFormDelegate delegate;

  private static final String POST_TEXT = "Write a post: ";
  private static final String POST_BUTTON_TEXT = "POST";

  public AddDiscussionForm (AddDiscussionFormDelegate delegate) {
    initializeLabel();
    initializeTextArea();
    initializeButton();
    initializeLayout();

    this.delegate = delegate;
  }

  private void initializeLabel () {
    this.postLabel = new MaterialJLabel();
    postLabel.setText(POST_TEXT);
    postLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
    postLabel.setMaximumSize(new Dimension(ViewConstants.DESKTOP_WIDTH - 60, 100));
  }

  private void initializeTextArea () {
    this.postBody = new MaterialJTextArea();
    postBody.setRows(10);
    postBody.setColumns(40);
    postBody.setLineWrap(true);
    postBody.setAlignmentX(Component.CENTER_ALIGNMENT);
    postBody.setMaximumSize(new Dimension(ViewConstants.DESKTOP_WIDTH - 60, 300));

  }

  private void initializeButton () {
    this.postButton = new MaterialJButton();
    postButton.setText(POST_BUTTON_TEXT);
    postButton.setMaximumSize(new Dimension(ViewConstants.DESKTOP_WIDTH, 20));
    postButton.addActionListener(actionEvent -> delegate.onPost(postBody.getText()));
  }

  private void initializeLayout () {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    add(Box.createRigidArea(new Dimension(0, 20)));
    add(postLabel);
    add(Box.createRigidArea(new Dimension(0, 5)));
    add(postBody);
    add(Box.createRigidArea(new Dimension(0, 10)));
    add(postButton);
  }
}
