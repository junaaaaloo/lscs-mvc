package edu.dlsu.lscs.view.desktop.components;

import edu.dlsu.lscs.model.discussions.Discussion;
import edu.dlsu.lscs.view.ViewConstants;
import edu.dlsu.lscs.view.components.MaterialJButton;
import edu.dlsu.lscs.view.components.MaterialJLabel;
import edu.dlsu.lscs.view.components.MaterialJTextArea;
import edu.dlsu.lscs.view.utility.FontManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.View;
import java.awt.*;

public class DiscussionItem extends JPanel {
  // Components
  private MaterialJLabel username;
  private MaterialJLabel postBody;

  private JPanel actionButtons;
  private JPanel titleData;


  private static final String DEFAULT_USERNAME = "@username";
  private static final String DEFAULT_POST_BODY = "This is a test, testing this. This is a test, testing this. This is a test, testing this. This is a test, testing this. This is a test. DONE";

  // Setup
  public DiscussionItem(Discussion discussion) {
    initializePanels();
    initializeLabels();
    initializeButtons();
    initializeLayout();

    setDiscussion(discussion);
  }

  private void initializePanels () {
    setBackground(ViewConstants.accentColor);

    actionButtons = new JPanel();
    actionButtons.setLayout(new FlowLayout(FlowLayout.TRAILING));
    actionButtons.setBackground(ViewConstants.accentColor);

    titleData = new JPanel();
    titleData.setLayout(new FlowLayout(FlowLayout.LEADING));
    titleData.setBackground(ViewConstants.accentColor);
  }

  private void initializeLabels () {
    username = new MaterialJLabel();
    username.setText(DEFAULT_USERNAME);
    username.setFont(FontManager.primaryFont().deriveFont(12.f).deriveFont(Font.BOLD));

    postBody = new MaterialJLabel();
    postBody.setText(DEFAULT_POST_BODY);

  }

  private void initializeButtons () {
  }

  private void initializeLayout () {
    setLayout(new BorderLayout(5, 5));
    setBorder(new EmptyBorder(10, 10, 10, 10));

    titleData.add(username);

    add(titleData, BorderLayout.NORTH);
    add(postBody, BorderLayout.CENTER);
    add(actionButtons, BorderLayout.SOUTH);
  }

  private void setDiscussion (Discussion discussion) {
    this.postBody.setText(discussion.getContent());
    this.username.setText("@" + discussion.getCreatedBy().getUsername());
  }
}
