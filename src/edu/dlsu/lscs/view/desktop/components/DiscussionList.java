package edu.dlsu.lscs.view.desktop.components;

import edu.dlsu.lscs.model.discussions.Discussion;
import edu.dlsu.lscs.model.discussions.DiscussionRepository;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class DiscussionList extends JPanel implements DiscussionRepository.DiscussionObserver {
  public interface DiscussionFetcher {
    List<Discussion> getDiscussions();
  }

  // View-Model
  private final List<DiscussionItem> discussionItems;

  public DiscussionList () {
    discussionItems = new ArrayList<>();

    initializeLayout();
  }

  public void initializeLayout () {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
  }

  @Override
  public void add(Discussion discussion) {
    DiscussionItem item = new DiscussionItem(discussion);
    discussionItems.add(item);

    add(Box.createRigidArea(new Dimension(0, 10)), 0);
    add(item, 0);
    repaint();
    revalidate();

  }
}
