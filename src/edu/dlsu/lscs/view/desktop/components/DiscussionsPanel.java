package edu.dlsu.lscs.view.desktop.components;

import edu.dlsu.lscs.model.discussions.Discussion;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class DiscussionsPanel extends JPanel implements AddDiscussionForm.AddDiscussionFormDelegate {
  public interface DiscussionsPanelDelegate {
    void onPost(String discussion);
  }

  private AddDiscussionForm addDiscussionForm;
  private DiscussionList discussionList;
  private final DiscussionsPanelDelegate delegate;

  public DiscussionsPanel (DiscussionsPanelDelegate delegate) {
    initializeAddDiscussion();
    initializeDiscussionList();
    initializeLayout();

    this.delegate = delegate;
  }

  public void initializeAddDiscussion () {
    this.addDiscussionForm = new AddDiscussionForm(this);
  }

  public void initializeDiscussionList () {
    this.discussionList = new DiscussionList();
  }

  public JScrollPane initializeScrollArea () {
    JScrollPane pane = new JScrollPane(discussionList);
    pane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    pane.setBorder(null);
    return pane;
  }

  public void initializeLayout () {
    setBorder(new EmptyBorder(10, 30, 10, 30));
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    add(addDiscussionForm);
    add(Box.createRigidArea(new Dimension(0, 50)));
    add(initializeScrollArea());
  }

  @Override
  public void onPost(String content) {
    delegate.onPost(content);
  }

  public DiscussionList getDiscussionList () {
    return discussionList;
  }
}
