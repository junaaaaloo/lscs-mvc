package edu.dlsu.lscs.view.desktop.components;

import edu.dlsu.lscs.model.user.User;
import edu.dlsu.lscs.model.user.UserRepository;
import edu.dlsu.lscs.view.ViewConstants;
import edu.dlsu.lscs.view.components.MaterialJButton;
import edu.dlsu.lscs.view.components.MaterialJLabel;
import edu.dlsu.lscs.view.components.MaterialJTextField;
import edu.dlsu.lscs.view.utility.FontManager;

import javax.swing.*;
import java.awt.*;

public class TopBarPanel extends JPanel implements UserRepository.UserObserver {
  public interface TopBarPanelDelegate {
    void onSignIn(String username);
    void onLogout();
  }
  private JPanel leftPanel;
  private MaterialJLabel talakayText;

  private JPanel rightPanel;
  private MaterialJLabel usernameText;
  private MaterialJTextField usernameField;
  private MaterialJButton signIn;
  private MaterialJButton logout;

  // Delegate
  private final TopBarPanelDelegate delegate;

  public static final int USERNAME_COLUMNS = 20;

  // Strings

  public static final String USERNAME_TEXT = "USERNAME";
  public static final String TALAKAY_TEXT = "Talakay";
  public static final String BUTTON_SIGN_IN = "SIGN IN";
  public static final String BUTTON_LOGOUT= "LOGOUT";

  // Setup Methods

  public TopBarPanel (TopBarPanelDelegate delegate) {
    initializePanels();
    initializeLabels();
    initializeField();
    initializeButtons();
    initializeLayout();

    this.delegate = delegate;
    this.loadNoUser();
  }

  private void initializePanels() {
    leftPanel = new JPanel();
    rightPanel = new JPanel();
    setBackground(ViewConstants.primaryColor);
    leftPanel.setBackground(ViewConstants.primaryColor);
    rightPanel.setBackground(ViewConstants.primaryColor);
  }

  private void initializeLabels () {
    usernameText = new MaterialJLabel();
    usernameText.setText(USERNAME_TEXT);
    usernameText.setForeground(ViewConstants.lightText);
    usernameText.setFont(FontManager.primaryFont().deriveFont(12.f).deriveFont(Font.BOLD));


    talakayText = new MaterialJLabel();
    talakayText.setText(TALAKAY_TEXT);
    talakayText.setForeground(ViewConstants.lightText);
    talakayText.setFont(FontManager.primaryFont()
                                   .deriveFont(18.f)
                                   .deriveFont(Font.ITALIC));
  }

  private void initializeField () {
    usernameField = new MaterialJTextField();
    usernameField.setForeground(ViewConstants.primaryColor);
    usernameField.setColumns(USERNAME_COLUMNS);
  }

  private void initializeButtons () {
    signIn = new MaterialJButton();
    signIn.setText(BUTTON_SIGN_IN);
    signIn.addActionListener(actionEvent -> delegate.onSignIn(usernameField.getText()));

    logout = new MaterialJButton();
    logout.setText(BUTTON_LOGOUT);
    logout.addActionListener(actionEvent -> delegate.onLogout());
  }

  private void initializeLayout () {
    rightPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
    rightPanel.add(usernameText);
    rightPanel.add(usernameField);
    rightPanel.add(signIn);
    rightPanel.add(logout);

    leftPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
    leftPanel.add(talakayText);

    setLayout(new BorderLayout());
    add(rightPanel, BorderLayout.EAST);
    add(leftPanel, BorderLayout.WEST);
  }

  @Override
  public void update(User user) {
    if (user == null) {
      loadNoUser();
    } else {
      loadWithUser(user.getUsername());
    }
  }

  public void loadNoUser () {
    this.usernameText.setText(USERNAME_TEXT);
    this.logout.setVisible(false);
    this.usernameField.setVisible(true);
    this.signIn.setVisible(true);
  }

  public void loadWithUser (String username) {
    this.usernameText.setText(username);
    this.logout.setVisible(true);
    this.usernameField.setVisible(false);
    this.signIn.setVisible(false);
  }
}
