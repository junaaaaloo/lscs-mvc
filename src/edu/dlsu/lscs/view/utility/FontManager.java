package edu.dlsu.lscs.view.utility;

import edu.dlsu.lscs.view.ViewConstants;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Singleton instance of the manager of fonts
 */
public class FontManager {
  private static final Logger LOG = Logger.getLogger(FontManager.class.getName());

  private static Font primaryFont;

  private FontManager () { };

  public static Font primaryFont () {
    if (primaryFont == null) {
      primaryFont = fetchPrimaryFont();
    }

    return primaryFont;
  }

  private static Font fetchPrimaryFont() {
    try {
      return Font.createFont(Font.TRUETYPE_FONT, new File(ViewConstants.PRIMARY_FONT_PATH));
    } catch ( IOException | FontFormatException e ) {
      LOG.warning("Font has not been loaded, returning null");
      return null;
    }
  }
}
