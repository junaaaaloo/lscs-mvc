package edu.dlsu.lscs.view.utility;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

public class IconManager {
  private static final Logger LOG = Logger.getLogger(IconManager.class.getName());

  private static Image icon;

  /**
   * This is a single instance, meaning that the FontManager is hidden.
   */
  private IconManager () { };

  public static Image icon () {
    if (icon == null) {
      icon = fetchIcon();
    }

    return icon;
  }

  private static Image fetchIcon() {
    try {
      return ImageIO.read(new File("resources/icon.png"));
    } catch ( IOException e ) {
      LOG.warning("Icon has not been loaded, returning null");
      return null;
    }
  }
}
